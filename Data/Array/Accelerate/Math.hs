{-#LANGUAGE TypeOperators #-}
-- | This module provides some basic vector operations.
module Data.Array.Accelerate.Math  where

import Data.Array.Accelerate.Types
import Data.Array.Unboxed
import Data.Array.Accelerate as Acc

-- | Returns the inverse of the given vector,
-- i.e. given (1,4,6), (1/1,1/4,1/6) is returned.
--
vectorInverseAcc :: (Elt a, IsFloating a) => AccVector a -> AccVector a
vectorInverseAcc v = Acc.map (1/) v

-- | Pairwise multiplication of two vectors.
--
(*^) :: (Elt a, IsFloating a) => AccVector a -> AccVector a -> AccVector a
(*^) = Acc.zipWith (*)

-- | Scalar multiplication of a vector.
--
(*.) :: (Elt a, IsFloating a) => AccScalar a -> AccVector a -> AccVector a
alpha *. v = Acc.map (the alpha*) v

-- | Scalar division of a vector.
-- Scalar is given as second argument.
--
(./) :: (Elt a, IsFloating a) => AccVector a -> AccScalar a -> AccVector a
v ./ alpha = Acc.map (the alpha/) v

-- | Scalar division of an arbitrary array.
-- Scalar is given as first argument.
--
(/.) :: (Elt a, IsFloating a, Shape sh) => AccScalar a -> Acc (Acc.Array sh a) -> Acc (Acc.Array sh a)
alpha /. v = Acc.map (the alpha/) v

-- | Dot product for two vectors.
-- Multiplies all elements pairwise and then sums them up.
--
dotpAcc :: (Elt a, IsFloating a, Shape ix) => Acc (Acc.Array (ix :. Int) a) -> Acc (Acc.Array (ix :. Int) a) -> Acc (Acc.Array ix a)
dotpAcc xs ys = Acc.fold (+) 0 (Acc.zipWith (*) xs ys)

-- | Dot product for two vectors. Wrapper for non-acc input.
-- See 'dotpAcc' for accelerate input.
--
dotp2Acc :: (Elt a, IsFloating a, Shape ix) => Acc.Array (ix :. Int) a -> Acc.Array (ix :. Int) a -> Acc (Acc.Array ix a)
dotp2Acc xs ys =
  let xs' = use xs
      ys' = use ys
  in
      dotpAcc xs' ys'

