{-#LANGUAGE ExplicitForAll,ScopedTypeVariables,TypeOperators #-}
-- | This module provides the definition of a sparse matrix
-- and a multiplication.
module Data.Array.Accelerate.Math.SMVM where

import Data.Array.Accelerate.Math.SMVM.Matrix
import Data.Array.Accelerate.Types

import Data.Array.Unboxed as AU hiding ((!))
--import Data.Array.Accelerate           (Vector, Segments, Acc, Array, use, Z, (:.))
import Data.Array.Accelerate
import Data.Array.Accelerate as A
import qualified Data.Array.Accelerate.Array.Sugar as Sugar
import qualified Data.Vector.Unboxed   as V
import Prelude hiding (replicate, zip, unzip, map, scanl, scanl1, scanr, scanr1, zipWith,
                         filter, max, min, not, fst, snd, curry, uncurry, sum, head, tail,
                         drop, take, null, length, reverse, init, last, product, minimum,
                         maximum)
import qualified Prelude

import Debug.Trace

-- * Types
-- -------

-- | Definition of a sparse vector, i.e. a tuple with the first
-- entry beeing the column of the entry, the second beeing the
-- entry itself, and the third entry is the original size of the
-- vector. To represent the vector (1,0,3) we would write
-- ([0,2],[1,3]) meaning that, the zeroth entry has value 1 and the second entry
-- has value 3. Note that the original length of the vector is not
-- preserved in this format. Thus, we store it in an additional field.
-- 
-- > let idx = fromList DIM1 [0,2] in
-- > let val = fromList DIM1 [1,3] in
-- >   (idx, val, 3)
--
type SparseVector a = (Vector Int, Vector a, Int)

-- | Definition of a sparse matrix based on the condensed-row format, i.e.
-- Segments as the number of non-zero element per row, and a 'SparseVector'
-- (without the size) covering the non-zero entries. The last entry
-- is the number of columns from the original matrix. The number of
-- rows can be extracted by length segments.
--
-- To encode ((1,0,2),(4,2,0),(0,0,1)) we store
-- ([2,2,1], ([0,2,0,1,2], [1,2,4,2,1]), 3).
--
type SparseMatrix a = (Segments Int, (Vector Int, Vector a), Int)

-- | Accelerate wrapper for 'SparseVector' type.
-- This type represent a sparse vector in the Accelerate DSL.
--
type AccSparseVector a = (AccVector Int, AccVector a, AccScalar Int)

-- | Accelerate Wrapper for 'SparseMatrix' type.
-- This type represents a sparse matrix in the Accelerate DSL.
--
type AccSparseMatrix a = (AccSegments, (AccVector Int, AccVector a), AccScalar Int)

-- * Utility functions
-- -----------------

-- ** Create sparse matrices

-- | Transfers a sparse matrix to accelerate by executing use for all components.
--
usesm :: (Elt a) => SparseMatrix a -> AccSparseMatrix a
usesm (segments, (vectors, values), size) = (use segments, (use vectors, use values), unit (constant size))

-- | Builds a sparse matrix from an Accelerate 'A.Array' with 0.0 as the
-- sparse element.
fromArrayZero :: forall a.(Eq a, Elt a, IsFloating a, Fractional a) => A.Array DIM2 a -> SparseMatrix a
fromArrayZero = fromArray (0.0 :: a)

-- | Builds a sparse matrix from an Accelerate 'A.Array' with an arbitrary sparse element.
fromArray :: (Eq a, Elt a) => a -> A.Array DIM2 a -> SparseMatrix a
fromArray zero arr = toArraySM $ transform arr
  where
    empty   = ([], ([], []),0)
    (_ :. row_min :. col_min, _ :. row_max :. col_max) = Sugar.shapeToRange $ arrayShape arr
    rows_i  = [row_min..row_max]
    cols_i  = [col_min..col_max]
    toArray l = fromList (Z :. Prelude.length l) l
    toArraySM (segList, (vecList, valList), cols) = (toArray segList, (toArray vecList, toArray valList), cols)
    --
    innerTransform arr r = foldr (\c (vec, val) ->
                                            let e = indexArray arr (Z :. r :. c) in
                                            if e == zero then
                                              (vec, val)
                                            else
                                              (c : vec, e : val)

                                          ) ([],[]) cols_i

    --
    transform arr = foldr (\r (seg, (vec, val), _) ->
                            let (vec1, val1) = innerTransform arr r
                            in
                             (Prelude.length vec1 : seg, (vec1 ++ vec, val1 ++ val), (col_max - col_min + 1))

                      ) empty rows_i

-- | Creates a unity matrix of size n*n
smunity :: Int -> SparseMatrix Float
smunity n = (segments, (vectors, values), n)
 where
   segments     = fromList (Z :. n) $ Prelude.take n $ repeat 1
   vectors      = fromList (Z :. n) [1..n]
   values       = fromList (Z :. n) $ Prelude.take n $ repeat 1.0

-- | Creates a unity matrix to be used in Accelerate.
smunity2Acc :: Int -> AccSparseMatrix Float
smunity2Acc n = (segments, (vectors, values), unit $ constant n)
 where
   segments     = use $ fromList (Z :. n) $ Prelude.take n $ repeat 1
   vectors      = use $ fromList (Z :. n) [1..n]
   values       = use $ fromList (Z :. n) $ Prelude.take n $ repeat 1.0

-- ** Properties

-- | Returns the row count of a given sparse matrix
-- See 'smrowsAcc' for accelerate version.
--
smrows :: Num a => SparseMatrix a -> Int
smrows (s, _, _) = arraySize (arrayShape s)

-- | Calculates the row count of an accelerate wrapped sparse matrix.
-- See 'smrows' for non-accelerate version.
--
smrowsAcc :: Num a => AccSparseMatrix a -> Exp Int
smrowsAcc (s, _, _) = size s

-- | Returns the col count of a given sparse matrix
-- See 'smcolsAcc' for accelerate version.
--
smcols :: Num a => SparseMatrix a -> Int
smcols (_, _, cols) = cols

-- | Calculates the col count of an accelerate wrapped sparse matrix.
-- See 'smcols' for non-accelerate version.
--
smcolsAcc :: Num a => AccSparseMatrix a -> Exp Int
smcolsAcc (_, _, cols) = the cols

-- * Mathematical functions
-- ----------------------

-- | Sparse-matrix vector multiplication
-- Multiplies a given sparse matrix and a vector in accelerate.
-- Note, that both inputs have to be already in the Accelerate space.
-- For a version starting with non-accelerate matrices and vectors
-- see 'smvm2Acc'.
smvmAcc :: AccSparseMatrix Float -> AccVector Float -> AccVector Float
smvmAcc (segd, (inds, vals), _) vec
  = let
      vecVals  = backpermute (shape inds) (\i -> index1 $ inds A.! i) vec
      products = A.zipWith (*) vecVals vals
    in
    foldSeg (+) 0 products segd

-- | Sparse-matrix vector multiplication. Wrapper for non-acc input.
-- This function transfers the input into the accelerate DSL
-- and then calls 'smvmAcc', i.e. calculation is done using accelerate.
smvm2Acc :: SparseMatrix Float -> Vector Float -> Acc (Vector Float)
smvm2Acc (segd', (inds', vals'), cols') vec'
  = let
      segd     = use segd'
      inds     = use inds'
      vals     = use vals'
      vec      = use vec'
      cols     = unit $ constant cols'
    in
      smvmAcc (segd, (inds, vals), cols) vec



-- TODO
-- smmul :: (Num a) => SparseMatrix a -> SparseMatrix a -> SparseVector a
-- smmul (seg1, (vec1, val1), (row1, col1)) (seg2, (vec2, val2), (row2, col2)) =
  

------------------------------

-- generateIndexValueMatrix

sparsedotpAcc :: AccSparseVector Float -> AccSparseVector Float -> AccScalar Float
sparsedotpAcc (idx1,val1,_) (idx2,val2,_) = A.foldAll (+) 0 $ A.map mapfun m1
  where
    v1 = A.zip idx1 val1 :: AccVector (Int, Float)
    v2 = A.zip idx2 val2 :: AccVector (Int, Float)
    genfun ix = let Z :. i :. j = unlift ix in lift (v1 A.! index1 i, v2 A.! index1 j)
    --
    m1 = generate (lift (Z :. size v1 :. size v2)) genfun
    --
    mapfun :: Exp ((Int, Float), (Int, Float)) -> Exp Float
    mapfun arg = let (ii,jj) = unlift arg :: (Exp (Int, Float), Exp (Int, Float)) in
                 let (i, vi) = unlift ii  :: (Exp Int, Exp Float) in
                 let (j, wj) = unlift jj  :: (Exp Int, Exp Float) in
                 (i ==* j) ? (vi * wj, constant 0)


-- | Sparse vector product. The returned vector may not be sparse, i.e. it may contain
-- values with value 0.0.
-- TODO Filter result array.
sparsevectorproductAcc :: AccSparseVector Float -> AccSparseVector Float -> AccSparseVector Float
sparsevectorproductAcc (idx1,val1,cnt1) (idx2,val2,_) = addcnt cnt1 $ unzip $ A.fold1 addfun $ A.map mapfun m1
  where
    v1 = A.zip idx1 val1 :: AccVector (Int, Float)
    v2 = A.zip idx2 val2 :: AccVector (Int, Float)
    genfun ix = let Z :. i :. j = unlift ix in lift (v1 A.! index1 i, v2 A.! index1 j)
    --
    m1 = generate (lift (Z :. size v1 :. size v2)) genfun
    --
    mapfun :: Exp ((Int, Float), (Int, Float)) -> Exp (Int,Float)
    mapfun arg = let (ii,jj) = unlift arg :: (Exp (Int, Float), Exp (Int, Float)) in
                 let (i, vi) = unlift ii  :: (Exp Int, Exp Float) in
                 let (j, wj) = unlift jj  :: (Exp Int, Exp Float) in
                 let res  = lift (i,vi * wj) :: Exp(Int, Float) in
                 let res2 = lift (i,0 :: Float) in
                 (i ==* j) ? (res, res2)
    --
    addcnt cnt (idx,val) = (idx,val,cnt)
    --
    addfun :: Exp (Int, Float) -> Exp (Int, Float) -> Exp (Int, Float)
    addfun ack val = let (acki, ackv) = unlift ack :: (Exp Int, Exp Float) in
                     let (i,v) = unlift val :: (Exp Int, Exp Float) in
                     lift (i, ackv + v) :: Exp (Int, Float)


-- Give comparison function!?
compareandswapAcc :: AccVector Float -> Exp Int -> Exp Int -> AccVector Float
compareandswapAcc x a b = backpermute (shape x) swap x
  where
    x_a = x ! index1 a
    x_b = x ! index1 b
    swap :: Exp (Z :. Int) -> Exp (Z :. Int)
    swap ix' =
      let ix = unindex1 ix' in
      ix ==* a &&* x_a >* x_b ? (index1 b, ix ==* b &&* x_a >* x_b ? (index1 a, ix'))
        
{-index2 :: Exp Int -> Exp Int -> Exp DIM2
index2 i j      = lift (Z :. i :. j)

unindex2 :: Exp DIM2 -> Exp (Int, Int)
unindex2 ix     = let Z :. i :. j = unlift (ix :: Exp DIM2) in lift ((i,j) :: (Exp Int, Exp Int))

unindex2' :: Exp DIM2 -> (Exp Int, Exp Int)
unindex2' ix     = let Z :. i :. j = unlift (ix :: Exp DIM2) in (i,j) :: (Exp Int, Exp Int)-}

----------------------

-- TODO Use IsElt instead of Float?
{-explodeSM :: AccSparseMatrix Float -> AccMatrix Float
explodeSM (seg,(idx,val),cols) = res
  where
    rows = size seg
    template = generate (index2 rows $ the cols) (\_ -> 0)
    res = permute
            (\a (\_ -> a))
            template
            (\ix -> ix)
            vals
            
-}

-- Maybe use [Exp Int] as input?! This cannot be used, can't it?
{-compareandswapMatrixAcc :: AccMatrix Float -> AccVector Int -> AccVector Int -> AccMatrix Float
compareandswapMatrixAcc x a b = backpermute (shape x) swap x
  where
    swap :: Exp (Z :. Int :. Int) -> Exp (Z :. Int :. Int)
    swap ix' = 
      let (ix1,ix2) = unindex2' ix' in
      let ix_a = (a ! index1 ix1) in
      let ix_b = (b ! index1 ix1) in
      let x_a = x ! index2 ix1 ix_a in
      let x_b = x ! index2 ix1 ix_b in
      ix2 ==* ix_a &&* x_a >* x_b ? (index2 ix1 ix_b, ix2 ==* ix_b &&* x_a >* x_b ? (index2 ix1 ix_a, ix'))


oddeven_merge x1 x2 hi lo r
  | step < hi - lo = x1
  | otherwise      = compareandswapAcc x lo (lo+r)
  where
    step = r * 2


oddeven_merge_sort_range :: AccVector Float -> Int -> Int -> AccVector Float
oddeven_merge_sort_range x hi lo 
  | (hi - lo) >= 1 = x
  | otherwise      = x
  where
    mid = lo + ((hi - lo) `div` 2)
    x1 = oddeven_merge_sort_range x lo mid
    x2 = oddeven_merge_sort_range x mid hi
    merge = oddeven_merge x1 x2 lo hi 1

oddeven_merge_sort :: AccVector Float -> AccVector Float
oddeven_merge_sort x = oddeven_merge_sort_range x 0 2

-}
